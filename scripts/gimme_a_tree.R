library(data.table)

###############################################################################
# functions
###############################################################################

collapse_func <- function(to_collapse) {
  tmp <- paste(to_collapse, collapse = ",")
  paste0("(", tmp, ")")
}

###############################################################################
# Import EukRibo table
###############################################################################

if (! dir.exists("data")) dir.create("data")

eukribo_table_url <- "https://zenodo.org/record/6896896/files/46346_EukRibo-02_2022-07-22.tsv.gz"

eukribo_table_dest <- "data/46346_EukRibo-02_2022-07-22.tsv.gz"

if (! file.exists(eukribo_table_dest)) {
  download.file(url = eukribo_table_url,
                destfile = eukribo_table_dest)
}

eukribo_table <- fread(eukribo_table_dest, sep = "\t", fill = TRUE)

# rm nucleomorphs
eukribo_table <- eukribo_table[!grepl("Z-nm-", taxogroup1)]

###############################################################################
# Extract unique path until taxogroup1
###############################################################################

taxo_path <- unique(eukribo_table[, list(UniEuk_taxonomy_string, taxogroup1)])

taxo_path <- apply(
  taxo_path,
  1,
  function(x) sub(paste0("(", x[2], ")\\|.+$"), "\\1", x[1])
)

taxo_path <- unique(taxo_path)

# merge micro-kingoms
taxo_path[grep("Eukaryota\\|X-", taxo_path)] <- "Eukaryota|potential-new-lineages"

taxo_path <- unique(taxo_path)

###############################################################################
# list taxo edges; a two columns table; col1 = taxo & col2 = child taxo
###############################################################################

taxo_edges <- tstrsplit(taxo_path, split = "\\|")

taxo_edges <- lapply(1:(length(taxo_edges) - 1), function(i){
  unique(data.table(taxo_edges[[i]], taxo_edges[[i + 1]]))
})

taxo_edges <- rbindlist(taxo_edges)

taxo_edges <- unique(taxo_edges[!(is.na(V1) | is.na(V2))])

setnames(taxo_edges,c("from","to"))

taxo_edges <- taxo_edges[seq_len(nrow(taxo_edges)) |> rev()]

taxo_edges[, to := sub("g:", "", to)]

###############################################################################
# transform into a newick tree
###############################################################################

res_list <- as.list(taxo_edges[! to %in% from,to])
names(res_list) <- taxo_edges[! to %in% from,to]

for (i in taxo_edges[, from] |> unique()){
  res_list[[i]]  <- res_list[taxo_edges[from == i, to]] |>
    unlist() |>
    collapse_func() |>
    paste0(i)
}

tree_nw <- paste0("(", res_list[[length(res_list)]], ");")
tree_nw <- ape::read.tree(text = tree_nw)

###############################################################################
# export
###############################################################################
if (! dir.exists("outputs")) dir.create("outputs")

ape::write.tree(phy = tree_nw,
                file = here::here("outputs", "46346_EukRibo-02_2022-07-22.tree"))
